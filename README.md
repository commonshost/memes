# memes 🖼

Keep track of funny images and video clips of sheep for use in blog posts, discussions, emails, tweets, and elsewhere.

Open a issue on this repository to share newly discovered treasures.

Close issues when using a meme in publication. Use memes only once to avoid stale content.

Attribute the source if possible. Avoid upsetting copyrightistas.
